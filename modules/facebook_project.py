
# coding: utf-8

# In[1]:


from fbrecog import FBRecog


# In[5]:


class FB(object):
    """
    To get the access token simply go to https://developers.facebook.com/tools/explorer 
    and get a user access token with user_photos, publish_actions and user_posts permissions.
    
    Get your Facebook cookie and fb_dtsg token as follows:
    1)Go to your Facebook profile.
    2)Open chrome dev tools by Right Click > Inspect
    3)Upload any picture. As it gets uploaded monitor the Network tab for 'dpr?' endpoint.
    4)Click on it. Scroll down to Request Header. Copy the entire cookie string.
    5)Scroll further down to Form Data. Copy the value of fb_dtsg parameter.
    """
    def __init__(self):
        self.access_token = 'EAACEdEose0cBAPvxzKfuP9s02JFFQHCLAZB0IORfGY1iWI8vSLUNZBy8t1NmIdTdMnUTMyu3aRFqhmqLnM9VgMpyt7acbTjW1SZAtHeWgV1QDppskRvgqzkQZAuQfZApZB7lP8GRe1SgLYcbFahmAbNmwg8kQr1jJQUjKVhws46iQUIF6sxMsQ80BAYKpbQxxOEXTwAVsewgZDZD' # Insert your access token obtained from Graph API explorer here
        self.cookies = 'sb=mer-WmLTHw9_mNsCQDF1Wo3C; datr=o-r-Wl4oo5n29figo69O_BgA; c_user=100003487212421; xs=43%3AL5BaKHr47wTJpw%3A2%3A1526655859%3A4750%3A14565; pl=n; wd=762x629; fr=0yOB59ikqOxKOp4Vl.AWXmDksn9TXM7ainXs9SJcQ-cXE.Ba-YQH.zH.FsB.0.0.BbBwPb.AWWV-qkg; act=1527186411274%2F0; presence=EDvF3EtimeF1527186413EuserFA21B03487212421A2EstateFDutF1527186413588CEchFDp_5f1B03487212421F2CC' # Insert your cookie string here
        self.fb_dtsg = 'AQE95r3yrhw3:AQH7Q6S0KHPN' # Insert the fb_dtsg parameter obtained from Form Data here.
    def set_token(self, access_token):
        self.access_token = access_token
    def set_cookies(self, cookies):
        self.cookies = cookies
    def set_fb_dtsg(self, fb_dtsg):
        self.fb_dtsg = fb_dtsg
    def fr_recognize(self, path):
        recog = FBRecog(self.access_token, self.cookies, self.fb_dtsg)
        output = recog.recognize_raw(path)
        name = output[0]['recognitions'][0]['user']['name']
        url_userpic = output[0]['recognitions'][0]['user']['profile_picture']
        fb_id = output[0]['recognitions'][0]['user']['fbid']
        prob = output[0]['recognitions'][0]['certainty']
        return {'name': name, 'url_userpic': url_userpic, 'id': fb_id, 'prob': prob}

